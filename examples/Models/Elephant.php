<?php

namespace com\Picorose\Examples;

use com\Picorose\DocFx\ArraySerialize;
use com\Picorose\Examples\interfaces\IAnimal;

/**
 * A single elephant. [Read more about elephants](https://en.wikipedia.org/wiki/Elephant)
 */
class Elephant implements IAnimal
{
    use ArraySerialize;

    // region Fields

    private float $height;
    private bool $hasTusks;

    // endregion

    // region Setup

    /**
     * Main constructor of the class
     *
     * @param float $height Height of the elephant in centimeters
     * @param bool $hasTusks Does the elephant still have its tusks
     */
    public function __construct(float $height, bool $hasTusks)
    {
        $this->height = $height;
        $this->hasTusks = $hasTusks;
    }

    // endregion

    // region Getters

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return "Elephant";
    }

    /**
     * @inheritDoc
     */
    public function getColor(): Color
    {
        return Color::GRAY;
    }

    // endregion

    // region Public

    /**
     * Elephants are herbivores, so they'll never eat another animal.
     *
     * @param IAnimal $other The animal to be eaten
     * @return bool Will always return false, as elephants don't eat other animals
     */
    public function doesEat(IAnimal $other): bool
    {
        return false;
    }

    // endregion
}