<?php

namespace com\Picorose\Examples;

use com\Picorose\DocFx\ArraySerialize;
use com\Picorose\Examples\interfaces\IAnimal;

/**
 * Representation of a human
 */
class Human implements IAnimal
{
    use ArraySerialize;

    // region Fields

    private string $name;
    private Color $color;
    private int $age;
    private bool $discloseAge;
    private bool $eatsMeat;

    // endregion

    // region Setup

    /**
     * Main constructor
     *
     * @param string $name The name of the human
     * @param Color $color The skin color of the human
     * @param int $age The age in full years of the human
     * @param bool $discloseAge Does the human want to disclose their age to the public
     * @param bool $eatsMeat Does the human eat meat
     */
    public function __construct(string $name, Color $color, int $age, bool $discloseAge, bool $eatsMeat)
    {
        $this->name = $name;
        $this->color = $color;
        $this->age = $age;
        $this->discloseAge = $discloseAge;
        $this->eatsMeat = $eatsMeat;
    }


    // endregion

    // region Getters

    /**
     * @return string The given name of the human
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Color The skin color of the human
     */
    public function getColor(): Color
    {
        return $this->color;
    }

    /**
     * @return int|bool The age in full years of the human or false if the human didn't want to disclose their age
     */
    public function getAge(): int|bool
    {
        if ($this->discloseAge)
            return $this->age;
        return false;
    }

    /**
     * @return bool True if the human eats meat
     */
    public function doesEatMeat(): bool
    {
        return $this->eatsMeat;
    }

    // endregion

    // region Setters

    /**
     * @param string $name The new name of the human
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param int|bool $age The new age of the human in full years
     */
    public function setAge(int|bool $age)
    {
        $this->age = $age;
    }

    /**
     * @param bool $eatsMeat True if the human eats meat, false if they do not
     */
    public function setEatsMeat(bool $eatsMeat)
    {
        $this->eatsMeat = $eatsMeat;
    }

    // endregion

    // region Setup

    /**
     * @inheritDoc
     */
    public function doesEat(IAnimal $other): bool
    {
        // Humans don't eat pets
        if ($other instanceof Pet)
            return false;

        return $this->eatsMeat;
    }

    // endregion
}