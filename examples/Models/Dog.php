<?php

namespace com\Picorose\Examples;

use com\Picorose\Examples\interfaces\IAnimal;

/**
 * Represents human's best friend, a dog
 */
class Dog extends Pet
{
    // region Constants

    const VARIABLE = "Blegh";
    public $mixedVal;
    public string $type = "Meh";

    // endregion

    // region Fields

    private Color $color;
    private string $breed;

    // endregion

    // region Setup

    /**
     * Main constructor for the Dog class
     *
     * @param Human|null $owner The owner of the dog
     * @param string|null $name The name of the dog
     * @param Color $color The fur color of the dog
     * @param string $breed The breed of the dog
     */
    public function __construct(?Human $owner, ?string $name, Color $color, string $breed)
    {
        parent::__construct($owner, $name);

        $this->color = $color;
        $this->breed = $breed;
    }

    // endregion

    // region Getters

    /**
     * @return Color The fur color of the dog
     */
    public function getColor(): Color
    {
        return $this->color;
    }

    /**
     * @return string The breed of the dog
     */
    public function getBreed(): string
    {
        return $this->breed;
    }

    // endregion

    // region Public

    /**
     * @inheritDoc
     */
    public function doesEat(IAnimal $other): bool
    {
        // Dogs aren't allowed to eat other pets, elephants are too big and they definitely don't eat humans
        if ($other instanceof Pet || $other instanceof Elephant || $other instanceof Human)
            return false;

        return true;
    }

    // endregion
}