<?php

namespace com\Picorose\Examples;

enum Color: string
{
    /**
     * Generic red color
     */
    case RED = "#FF0000";

    /**
     * Generic green color
     */
    case GREEN = "#00FF00";

    /**
     * Generic blue color
     */
    case BLUE = "#0000FF";

    /**
     * Generic yellow color
     */
    case YELLOW = "#FFFF00";

    /**
     * Generic magenta color
     */
    case MAGENTA = "#FF00FF";

    /**
     * Generic cyan color
     */
    case CYAN = "#00FFFF";

    /**
     * Generic orange color
     */
    case ORANGE = "#FF9900";

    /**
     * Dark gray color for elephants
     */
    case GRAY = "#333333";

    /**
     * Light human skin color
     */
    case LIGHT_PINK = "#EBCED7";

    /**
     * Dark human skin color
     */
    case DARK_BROWN = "#1A0900";
}