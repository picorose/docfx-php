<?php

namespace com\Picorose\Examples;

use com\Picorose\DocFx\ArraySerialize;
use com\Picorose\Examples\interfaces\IAnimal;

/**
 * A pet animal that can be kept by a human
 */
abstract class Pet implements IAnimal
{
    use ArraySerialize;

    // region Fields

    private ?Human $owner;
    private ?string $name;

    // endregion

    // region Setup

    public function __construct(?Human $owner, ?string $name)
    {
        $this->owner = $owner;
        $this->name = $name;
    }

    // endregion

    // region Getters

    /**
     * @return Human|null The owner of the pet, or null if the pet no longer has an owner
     */
    public function getOwner(): ?Human
    {
        return $this->owner;
    }

    /**
     * @return string The name that was given to the pet
     */
    public function getName(): string
    {
        return $this->name;
    }

    // endregion
}