<?php

namespace com\Picorose\Examples\interfaces;

interface IFlyingAnimal extends IAnimal
{
    // region Public

    public function canFly(string $location): bool;

    // endregion
}