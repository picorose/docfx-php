<?php

namespace com\Picorose\Examples;

use com\Picorose\Examples\interfaces\IAnimal;
use com\Picorose\Examples\interfaces\IFlyingAnimal;

class Parrot extends Pet implements IFlyingAnimal
{
    // region Fields

    private Color $color;
    private bool $wingsClipped;
    private int $age;

    // endregion

    // region Setup

    /**
     * Main constructor of the Parrot class
     *
     * @param Human|null $owner The owner of the parrot
     * @param string|null $name The name of the parrot
     * @param Color $color The main feather color of the parrot
     * @param bool $wingsClipped Does the parrot have its wings clipped
     * @param int $age The age of the parrot in full years
     */
    public function __construct(?Human $owner, ?string $name, Color $color, bool $wingsClipped, int $age)
    {
        parent::__construct($owner, $name);

        $this->color = $color;
        $this->wingsClipped = $wingsClipped;
        $this->age = $age;
    }

    // endregion

    // region Getters

    /**
     * @return Color The main feather color of the parrot
     */
    public function getColor(): Color
    {
        return $this->color;
    }

    /**
     * @return bool Does the parrot have its wings clipped
     */
    public function areWingsClipped(): bool
    {
        return $this->wingsClipped;
    }

    /**
     * @return int The age of the parrot in full years
     */
    public function getAge(): int
    {
        return $this->age;
    }

    // endregion

    // region Setters

    /**
     * @param bool $wingsClipped True if the parrot has its wings clipped
     */
    public function setWingsClipped(bool $wingsClipped)
    {
        $this->wingsClipped = $wingsClipped;
    }

    /**
     * @param int $age The new age of the parrot in full years
     */
    public function setAge(int $age)
    {
        $this->age = $age;
    }

    // endregion

    // region Public

    /**
     * Parrots are herbivores, so they don't eat other animals
     *
     * @param IAnimal $other The animal to be eaten
     * @return bool Always returns false, because parrots are herbivores
     */
    public function doesEat(IAnimal $other): bool
    {
        return false;
    }

    /**
     * If the parrot is in a cage or has its wings clipped it can not fly, otherwise it can and it will.
     *
     * @param string $location The current location of the parrot
     * @return bool True if the parrot can fly in the given location
     */
    public function canFly(string $location): bool
    {
        if ($this->wingsClipped)
            return false;

        if ($location == "cage")
            return false;

        return true;
    }

    // endregion
}