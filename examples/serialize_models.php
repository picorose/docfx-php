<?php
/*
use com\Picorose\DocFx\ClassParser;
use ZipStream\Option\Archive;
use ZipStream\Option\File;
use ZipStream\ZipStream;

function loadFiles(string $path)
{
    if (!is_dir($path))
        return;

    // Loop through each file in the given path
    foreach (scandir($path) as $file) {
        if ($file == "." || $file == "..")
            continue;

        // Only load php files
        if (strlen($file) > 4 && strtolower(substr($file, -4)) == ".php")
            require_once realpath($path) . "/$file";

        // Recursively load files in sub-directories
        if (is_dir($file))
            loadFiles($path . "/$file");
    }
}

// Load composer dependencies
require_once realpath(__DIR__ . "/../vendor/") . "/autoload.php";

// Load example models
loadFiles(realpath(__DIR__ . "/Models"));

// Load DocFx-php source
loadFiles(realpath(__DIR__ . "/../src/"));

// Parse all classes in the examples namespace and export the yaml files in a zip file
if (empty(realpath(__DIR__ . "/../build")))
    mkdir(realpath(__DIR__ . "/../") . "/build");

ClassParser::parseNamespaceAsZip("com\\Picorose\\Examples", realpath(__DIR__ . "/../build") . "/output.zip", false);*/

use com\Picorose\DocFx\ClassParser;
use com\Picorose\DocFx\TableOfContents;

$GLOBALS["namespace"] = "com\\Picorose\\Examples";

require_once realpath(__DIR__ . "/../vendor/") . "/autoload.php";

spl_autoload_register(function ($class_name) {
    if (str_starts_with($class_name, $GLOBALS["namespace"]))
        $class_name = substr($class_name, strlen($GLOBALS["namespace"]) + 1);

    if (file_exists(__DIR__ . "/Models/$class_name.php"))
        require_once __DIR__ . "/Models/$class_name.php";
});

// Recursively load classes from folders, assuming the PSR-4 standards are used
$namespaces = [];
loadClass(__DIR__ . "/Models/", $GLOBALS["namespace"], $namespaces);

// Remove old target folder
if (!empty(realpath(__DIR__ . "/../target")))
    rrmdir(realpath(__DIR__ . "/../target"));

mkdir(realpath(__DIR__ . "/../") . "/target");

// Parse classes and save to target folder
/** @var TableOfContents[] $tocs */
$tocs = [];
generateDocs($GLOBALS["namespace"], $tocs);
foreach ($namespaces as $ns)
    generateDocs($ns, $tocs);

$concatToc = new TableOfContents();
foreach ($tocs as $toc)
    foreach ($toc->getItems() as $item)
        if (count($item->getItems()) > 0)
            $concatToc->addItem($item);

file_put_contents(realpath(__DIR__ . "/../target") . "/{$concatToc->getUid()}.yml", $concatToc->toYaml());

echo "\e[0;32mSuccessfully parsed classes!\e[0m";

function loadClass(string $path, string $namespace, array &$namespaces)
{
    if (!is_dir($path))
        return;

    // Loop through each file in the given path
    foreach (scandir($path) as $file) {
        if ($file == "." || $file == "..")
            continue;

        // Only load php files
        if (str_ends_with(strtolower($file), ".php")) {
            $class = substr("$namespace\\$file", 0, -4);
            $ref = new ReflectionClass($class);
        }

        // Recursively load files in sub-directories
        if (is_dir("$path\\$file")) {
            $ns = "$namespace\\$file";
            if (!in_array($ns, $namespaces))
                $namespaces[] = $ns;

            loadClass("$path\\$file", $ns, $namespaces);
        }
    }
}

function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object))
                    rrmdir($dir . DIRECTORY_SEPARATOR . $object);
                else
                    unlink($dir . DIRECTORY_SEPARATOR . $object);
            }
        }
        rmdir($dir);
    }
}

function generateDocs(string $namespace, array &$tocs)
{
    $refs = ClassParser::parseNamespace($namespace, true);
    foreach ($refs as $item) {
        if ($item instanceof TableOfContents)
            $tocs[] = $item;
        else
            file_put_contents(realpath(__DIR__ . "/../target") . "/{$item->getUid()}.yml", $item->toYaml());
    }
}