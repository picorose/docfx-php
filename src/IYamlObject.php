<?php
/**
 * Copyright (c) Picorose
 * Licensed under the MIT license. See LICENSE file in the project root for full license information
 *
 * @author Colin Rosen
 * @date 2022
 * @since 1.0.0
 */

namespace com\Picorose\DocFx;

/**
 * An object that can be converted to a yaml string
 */
interface IYamlObject
{
    // region Public

    /**
     * @return string A yaml representation of this object
     */
    public function toYaml(): string;

    // endregion
}