<?php
/**
 * Copyright (c) Picorose
 * Licensed under the MIT license. See LICENSE file in the project root for full license information
 *
 * @author Colin Rosen
 * @date 2022
 * @since 1.0.0
 */

namespace com\Picorose\DocFx;

use Symfony\Component\Yaml\Yaml;

/**
 * Representation of the table of contents of a DocFX project
 */
class TableOfContents implements IYamlObject
{
    // region Fields

    private array $items = [];

    // endregion

    // region Getters

    /**
     * @return string The uid of the object. It will always just be 'toc'.
     */
    public function getUid(): string
    {
        return "toc";
    }

    /**
     * @return TableItem[] A list of TOC items in this table
     */
    public function getItems(): array
    {
        return $this->items;
    }

    // endregion

    // region Setters

    /**
     * @param TableItem[] $items A list of TOC items in this table
     */
    public function setItems(array $items)
    {
        $this->items = $items;
    }

    /**
     * Adds an item to this table
     *
     * @param TableItem $item The TOC item to add
     */
    public function addItem(TableItem $item)
    {
        $this->items[] = $item;
    }

    // endregion

    // region Public

    /**
     * @inheritDoc
     */
    public function toYaml(): string
    {
        $its = [];
        foreach ($this->items as $it)
            $its[] = $it->toArray();

        $header = "### YamlMime:TableOfContent\n";
        $yaml = Yaml::dump($its, 6, 2);
        return $header . $yaml;
    }

    // endregion
}