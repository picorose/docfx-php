<?php
/**
 * Copyright (c) Picorose
 * Licensed under the MIT license. See LICENSE file in the project root for full license information
 *
 * @author Colin Rosen
 * @date 2022
 * @since 1.0.0
 */

namespace com\Picorose\DocFx;

use phpDocumentor\Reflection\DocBlockFactory;
use ReflectionClass;
use ReflectionClassConstant;
use ReflectionEnum;
use ReflectionEnumBackedCase;
use ReflectionException;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionProperty;
use ReflectionType;
use ReflectionUnionType;
use ZipStream\Option\Archive;
use ZipStream\ZipStream;

/**
 * Utility for parsing a class or a namespace and its classes into a format that can be used with DocFX
 */
class ClassParser
{
    // region Public

    /**
     * Parses the given namespace and all loaded classes with the given namespace into {@see ManagedReference}s. A
     * {@see TableOfContents} object is also included for the generated references.
     *
     * @param string $namespace The namespace to serialize
     * @param bool $preciseNamespace True if the given namespace should exactly match the namespace of the class for it to parse
     * @return IYamlObject[] An array containing the {@see TableOfContents}, namespace {@see ManagedReference} and
     * classes {@see ManagedReference} objects. All of the files can be converted to a yaml string using the
     * {@see IYamlObject::toYaml()} function.
     */
    public static function parseNamespace(string $namespace, bool $preciseNamespace = false): array
    {
        $classes = [];
        $references = [];
        foreach (array_merge(get_declared_classes(), get_declared_interfaces()) as $class) {
            $ref = new ReflectionClass($class);

            if ($preciseNamespace && $ref->getNamespaceName() !== $namespace)
                continue;

            if (!$preciseNamespace && !str_starts_with($ref->getNamespaceName(), $namespace))
                continue;

            $cz = self::parseClass($class);
            $classes[] = $cz;
            $references[] = new Reference($cz->getItems()[0]->getUid(), $cz->getItems()[0]->getName(), "", $cz->getItems()[0]->getFullName());
        }

        $ns = new Item();
        $ns->setUid(str_replace("\\", ".", $namespace));
        $ns->setId($ns->getUid());
        foreach ($classes as $class)
            $ns->addChild($class->getItems()[0]->getUid());
        $ns->setLangs(["php"]);
        $ns->setName($namespace);
        $ns->setNameWithType($namespace);
        $ns->setFullName($namespace);
        $ns->setType("Namespace");
        $ns->setSyntax(new Syntax("namespace $namespace;"));

        $ref = new ManagedReference();
        $ref->setItems([$ns]);
        $ref->setReferences($references);

        $toc = new TableOfContents();
        $nsIt = new TableItem();
        $nsIt->setTopicUid($ns->getUid());
        $nsIt->setName($ns->getName());
        foreach ($classes as $class) {
            $it = new TableItem();
            $it->setTopicUid($class->getItems()[0]->getUid());
            $it->setName($class->getItems()[0]->getName());
            $nsIt->addItem($it);
        }
        $toc->addItem($nsIt);

        return [$toc, $ref, ...$classes];
    }

    /**
     * Parses the given class into a {@see ManagedReference} object
     *
     * @param string $class The FQDN of the class that is to be serialized
     * @return ManagedReference A representation of the given class that can be used in the DocFX system.
     * @throws ReflectionException If the given classname is invalid or not defined
     */
    public static function parseClass(string $class): ManagedReference
    {
        $class = new ReflectionClass($class);
        if ($class->isEnum())
            return self::parseEnum($class);

        // Set basic data
        $classItem = new Item();
        $classItem->setUid(self::classToUid($class->getName()));
        $classItem->setId($class->getShortName());
        $classItem->setParent(str_replace("\\", ".", $class->getNamespaceName()));
        $classItem->setLangs(["php"]);
        $classItem->setName($class->getShortName());
        $classItem->setNameWithType($class->getShortName());
        $classItem->setFullName($class->getName());
        $classItem->setNamespace($class->getNamespaceName());

        // Set item type
        if ($class->isInterface())
            $classItem->setType("Interface");
        else
            $classItem->setType("Class");
        if ($class->isAbstract())
            $classItem->addModifier("abstract");

        // Set summary
        $fac = DocBlockFactory::createInstance();
        if ($class->getDocComment())
            $doc = $fac->create($class->getDocComment());
        else
            $doc = $fac->create("/**  */");
        $summary = $doc->getSummary() . $doc->getDescription();
        foreach ($doc->getTags() as $tag)
            $summary .= "<br />\n<b>" . $tag->getName() . "</b>: " . $tag;
        $classItem->setSummary($summary);

        // Serialize methods and public fields/constants in the class
        list($methods, $mRefs, $inheritedMembers) = self::serializeMethods($class);
        list($fields, $fRefs) = self::serializeFields($class, $mRefs);

        // Set inherited methods from parent classes and add method and field items as children
        $classItem->setInheritedMembers($inheritedMembers);
        $classItem->setChildren(array_merge(array_keys($methods), array_keys($fields)));

        $classHead = implode(" ", $classItem->getModifiers()) . " " . strtolower($classItem->getType()) . " " . $class->getShortName();
        $classHead = trim($classHead);
        if ($class->getParentClass()) {
            $prnt = explode("\\", $class->getParentClass()->getName());
            $prnt = $prnt[count($prnt) - 1];
            $classHead .= " extends " . $prnt;
        }
        if (!empty($class->getInterfaceNames())) {
            $ifaces = $class->getInterfaceNames();
            if ($class->getParentClass())
                foreach ($class->getParentClass()->getInterfaceNames() as $if) {
                    $index = array_search($if, $ifaces);
                    if ($index !== false)
                        array_splice($ifaces, $index, 1);
                }

            if (!empty($ifaces)) {
                // Remove namespace
                for ($i = 0; $i < count($ifaces); $i++) {
                    $if = explode("\\", $ifaces[$i]);
                    $if = $if[count($if) - 1];
                    $ifaces[$i] = str_replace("\\", ".", $if);
                }

                if ($class->isInterface())
                    $classHead .= " extends " . implode(", ", $ifaces);
                else
                    $classHead .= " implements " . implode(", ", $ifaces);
            }

            $implements = $class->getInterfaceNames();
            for ($i = 0; $i < count($implements); $i++) {
                $ns = explode("\\", $implements[$i]);
                $ns = substr($implements[$i], 0, -strlen($ns[count($ns) - 1]) - 1);

                $shortType = explode("\\", $implements[$i]);
                $shortType = $shortType[count($shortType) - 1];
                $longType = $implements[$i];

                $ref = new Reference(self::classToUid($longType), $shortType, "", $longType, $shortType, [], $ns != $class->getNamespaceName());

                self::addReferenceType($ref, $fRefs);
                $implements[$i] = self::classToUid($implements[$i]);
            }

            $classItem->setImplements($implements);
        }
        $classItem->setSyntax(new Syntax($classHead));

        if ($class->getParentClass())
            self::addExtension($classItem, $class->getName());

        $ref = new ManagedReference();
        $ref->setItems([$classItem, ...array_values($methods), ...array_values($fields)]);
        $ref->setReferences(array_values($fRefs));

        return $ref;
    }

    /**
     * Parses the given namespace and all loaded classes with the given namespace into {@see ManagedReference}s. A
     * {@see TableOfContents} object is also included for the generated references.
     * After parsing the classes and namespaces each object will be serialized into a yaml string and added to a zip
     * archive. The necessary headers will be set to download the resulting zip archive.
     *
     * @param string $namespace The namespace to serialize
     * @param string $filename The filename of the zip file that will be generated
     * @param bool $sendHttpHeaders Enable or disable output of HTTP headers for download
     * @throws \ZipStream\Exception\OverflowException
     */
    public static function parseNamespaceAsZip(string $namespace, string $filename = "file.zip", bool $sendHttpHeaders = true)
    {
        $classItems = ClassParser::parseNamespace($namespace);

        if ($sendHttpHeaders) {
            $options = [
                "sendHttpHeaders" => $sendHttpHeaders,
                "outputName" => basename($filename)
            ];
        } else {
            $options = [
                "outputStream" => fopen($filename, 'w'),
                "enableZip64" => false
            ];
        }

        // Create zipstream
        $zip = new ZipStream(...$options);
        foreach ($classItems as $item)
            $zip->addFile($item->getUid() . ".yml", $item->toYaml());
        $zip->finish();
    }

    // endregion

    // region Private

    private static function classToUid(string $class): string
    {
        $parts = explode("|", $class);
        $result = "";
        foreach ($parts as $part) {
            if (!empty($result))
                $result .= "|";

            if (!str_contains($part, "\\") && !str_contains($part, "."))
                $part = "php.$part";
            $part = str_replace("\\", ".", $part);
            $result .= $part;
        }

        return $result;
    }

    private static function parseEnum(ReflectionClass $class): ManagedReference
    {
        // Set basic data
        $classItem = new Item();
        $classItem->setUid(self::classToUid($class->getName()));
        $classItem->setId($class->getShortName());
        $classItem->setParent(str_replace("\\", ".", $class->getNamespaceName()));
        $classItem->setLangs(["php"]);
        $classItem->setName($class->getShortName());
        $classItem->setNameWithType($class->getShortName());
        $classItem->setFullName($class->getName());
        $classItem->setNamespace($class->getNamespaceName());
        $classItem->setType("Enum");

        // Set summary
        $fac = DocBlockFactory::createInstance();
        if ($class->getDocComment())
            $doc = $fac->create($class->getDocComment());
        else
            $doc = $fac->create("/**  */");
        $summary = $doc->getSummary() . $doc->getDescription();
        foreach ($doc->getTags() as $tag) {
            $summary .= "<br />\n<b>" . $tag->getName() . "</b>: " . $tag;
        }
        $classItem->setSummary($summary);

        $enum = new ReflectionEnum($class->getName());

        // Get cases for enum
        $cases = [];
        foreach ($enum->getCases() as $case) {
            $item = new EnumCase();
            $item->setUid($class->getShortName() . "." . $case->getName());
            $item->setId($case->getName());
            $item->setName($case->getName());
            $item->setType("Field");
            if ($case instanceof ReflectionEnumBackedCase) {
                $item->setSyntax(new Syntax($case->getBackingValue() . ""));
                $item->setValue($case->getBackingValue());
            }
            $item->setFullName(self::classToUid($class->getName()) . "." . $case->getName());

            $fac = DocBlockFactory::createInstance();
            if ($case->getDocComment())
                $doc = $fac->create($case->getDocComment());
            else
                $doc = $fac->create("/**  */");
            $item->setSummary($doc->getSummary());

            $cases[$item->getUid()] = $item;
        }
        $classItem->setChildren(array_keys($cases));

        // Set syntax
        $classHead = implode(" ", $classItem->getModifiers()) . " " . strtolower($classItem->getType()) . " " . $class->getShortName();
        $classHead = trim($classHead);
        if ($enum->getBackingType())
            $classHead .= " : " . $enum->getBackingType();

        $classItem->setSyntax(new Syntax($classHead));

        $implements = $class->getInterfaceNames();
        $fRefs = [];
        for ($i = 0; $i < count($implements); $i++) {
            $ns = explode("\\", $implements[$i]);
            $ns = substr($implements[$i], 0, -strlen($ns[count($ns) - 1]) - 1);

            $shortType = explode("\\", $implements[$i]);
            $shortType = $shortType[count($shortType) - 1];
            $longType = $implements[$i];

            $ref = new Reference(self::classToUid($longType), $shortType, "", $longType, $shortType, [], $ns != $class->getNamespaceName());

            self::addReferenceType($ref, $fRefs);
            $implements[$i] = self::classToUid($implements[$i]);
        }

        $classItem->setImplements($implements);

        if ($class->getParentClass())
            self::addExtension($classItem, $class->getName());

        $ref = new ManagedReference();
        $ref->setItems([$classItem, ...array_values($cases)]);
        $ref->setReferences(array_values($fRefs));

        return $ref;
    }

    private static function serializeMethods(ReflectionClass $class): array
    {
        $references = [];
        $arr = [];
        $inherited = [];

        $fac = DocBlockFactory::createInstance();
        foreach ($class->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            // Check if method is inherited from a parent class
            if ($method->getDeclaringClass() != $class) {
                // Don't include constructor in the list of inherited members
                if ($method->isConstructor())
                    continue;

                $shortPars = [];
                $longPars = [];
                foreach ($method->getParameters() as $par) {
                    if ($par->hasType())
                        $ref = self::typeToReference($par->getType(), $class->getNamespaceName());
                    else
                        $ref = new Reference("php.mixed", "", "", "mixed", "", [], true);

                    $shortPars[] = $ref->getName();
                    $longPars[] = $ref->getFullName();
                }

                $uid = self::classToUid($method->getDeclaringClass()->getName()) . "." . $method->getName() . "(" . implode(",", $shortPars) . ")";
                $name = $method->getName() . "(" . implode(",", $shortPars) . ")";
                $fullName = $method->getDeclaringClass()->getName() . "->" . $method->getName() . "(" . implode(",", $longPars) . ")";
                $nameWithType = $method->getDeclaringClass()->getShortName() . "->" . $method->getName() . "(" . implode(",", $shortPars) . ")";
                $inherited[] = $uid;

                if (!array_key_exists($uid, $references))
                    $references[$uid] = new Reference($uid, $name, "", $fullName, $nameWithType);
                continue;
            }

            list($doc, $docPars) = self::getMethodSummary($fac, $method);

            $className = self::classToUid($class->getName());
            $shortName = $method->getName();

            // Compile parameters
            $pars = [];
            $longPars = [];
            foreach ($method->getParameters() as $par) {
                $allowsNull = false;
                if ($par->hasType()) {
                    $ref = self::typeToReference($par->getType(), $class->getNamespaceName());
                    $allowsNull = $par->getType()->allowsNull();
                } else
                    $ref = new Reference("php.mixed", "", "", "mixed", "", [], true);

                self::addReferenceType($ref, $references);

                $pars[] = $ref->getName();
                $comment = "";
                if (array_key_exists($par->getName(), $docPars))
                    $comment = $docPars[$par->getName()]->getDescription();
                $longPars[] = new Type($par->getName(), $ref->getFullName(), $allowsNull, $comment);
            }
            $shortName .= "(" . implode(",", $pars) . ")";
            $name = "$className.$shortName";

            // Set up basic item data
            $item = new Item();
            $item->setUid($name);
            $item->setId($shortName);
            $item->setParent($className);
            $item->setLangs(["php"]);
            $item->setName($method->getName() . "(" . implode(", ", $longPars) . ")");
            $item->setNameWithType($class->getShortName() . "->" . $item->getName());
            $item->setFullName($class->getName() . "->" . $item->getName());
            $item->setSummary($doc->getSummary());
            if ($method->isConstructor())
                $item->setType("Constructor");
            else
                $item->setType("Method");
            $item->setNamespace($class->getNamespaceName());

            // Add method modifiers
            $item->addModifier("public");
            if ($method->isAbstract())
                $item->addModifier("abstract");
            if ($method->isStatic())
                $item->addModifier("static");
            if ($method->isFinal())
                $item->addModifier("final");

            // Setup syntax data
            $syntax = new Syntax();
            $content = implode(" ", $item->getModifiers()) . " function " . $item->getName();

            // If the method has a return type, add it to the syntax
            if ($method->getReturnType()) {
                $type = $method->getReturnType();
                $ref = self::typeToReference($type, $method->getDeclaringClass()->getNamespaceName());

                $content .= ": " . $ref->getName();
                $type = new Type();
                $type->setType(self::classToUid($ref->getFullName()));
                $tags = $doc->getTagsWithTypeByName("return");
                if (!empty($tags))
                    $type->setDescription($tags[0]->getDescription());
                $syntax->setReturn($type);

                // Make sure each referenced type is saved in the references
                self::addReferenceType($ref, $references);
            }
            $syntax->setContent($content);
            foreach ($longPars as $par)
                $syntax->addParameter(new Type($par->getId(), self::classToUid($par->getType()), $par->allowsNull(), $par->getDescription()));
            $item->setSyntax($syntax);

            // Check if method overrides a method in the parent class
            if ($class->getParentClass() && $class->getParentClass()->hasMethod($method->getName())) {
                $methodClass = $class->getParentClass()->getMethod($method->getName())->getDeclaringClass();
                if (!$methodClass->isInterface() && !$method->isConstructor()) {
                    $methodClass = self::classToUid($methodClass->getName());
                    $item->setOverridden("$methodClass." . $item->getName());
                }
            }

            $arr[$name] = $item;
        }

        return [$arr, $references, $inherited];
    }

    private static function serializeFields(ReflectionClass $class, array $references): array
    {
        $props = $class->getProperties(ReflectionProperty::IS_PUBLIC);
        $props = array_merge($props, $class->getReflectionConstants(ReflectionClassConstant::IS_PUBLIC));

        $arr = [];
        foreach ($props as $field) {
            $item = new Item();
            $item->setType("Field");

            $fac = DocBlockFactory::createInstance();
            if ($field->getDocComment())
                $doc = $fac->create($field->getDocComment());
            else
                $doc = $fac->create("/**  */");
            $summary = $doc->getSummary() . $doc->getDescription();
            foreach ($doc->getTags() as $tag)
                $summary .= "<br />\n<b>" . $tag->getName() . "</b>: " . $tag;
            $item->setSummary($summary);

            $className = self::classToUid($class->getName());
            $name = $field->getName();
            $item->setUid("$className.$name");
            $item->setId($name);
            $item->setName($name);

            if ($field instanceof ReflectionProperty) {
                if ($field->isStatic())
                    $item->addModifier("static");
                if ($field->isReadOnly())
                    $item->addModifier("readonly");
                $mods = implode(" ", $item->getModifiers());
                if (!empty($mods))
                    $mods .= " ";
                if ($field->hasType())
                    $ref = self::typeToReference($field->getType(), $field->getDeclaringClass()->getNamespaceName());
                else
                    $ref = new Reference("php.mixed", "mixed", "", "mixed", "mixed", [], true);
                $content = "public " . $mods . $ref->getName() . " $" . $field->getName();
                if ($field->hasDefaultValue() && $field->getDefaultValue() . "" != "") {
                    if (is_string($field->getDefaultValue()))
                        $content .= " = \"" . $field->getDefaultValue() . "\"";
                    else
                        $content .= " = " . $field->getDefaultValue();
                }

                $return = new Type();
                $return->setType(self::classToUid($ref->getFullName()));
                $return->setDescription($item->getSummary());
                $item->setSummary("");
                $syntax = new Syntax($content, $return);
                $item->setSyntax($syntax);

                self::addReferenceType($ref, $references);
            } else {
                $content = "const " . $field->getName();
                if (is_string($field->getValue()))
                    $content .= " = \"" . $field->getValue() . "\"";
                else if (is_array($field->getValue()))
                    $content .= " = " . json_encode($field->getValue());
                else
                    $content .= " = " . $field->getValue();
                $item->setSyntax(new Syntax($content));
            }

            $arr[$item->getUid()] = $item;
        }

        return [$arr, $references];
    }

    private static function addExtension(Item $classItem, string $currentClass)
    {
        $obj = new ReflectionClass($currentClass);
        $classItem->addInheritance(self::classToUid($obj->getParentClass()->getName()));

        if ($obj->getParentClass()->getParentClass())
            self::addExtension($classItem, $obj->getParentClass()->getName());
    }

    private static function addReferenceType(Reference $ref, array &$references)
    {
        if (array_key_exists(trim($ref->getFullName()), $references))
            return;

        $references[$ref->getFullName()] = $ref;
    }

    private static function getMethodSummary(DocBlockFactory $factory, ReflectionMethod $method): array
    {
        if ($method->getDocComment())
            $doc = $factory->create($method->getDocComment());
        else
            $doc = $factory->create("/** */");

        // @inheritDoc tag is set, use the docs from the parent class
        if ($doc->hasTag("inheritDoc")) {
            if ($method->getDeclaringClass()->getParentClass() && $method->getDeclaringClass()->getParentClass()->hasMethod($method->getName()))
                return self::getMethodSummary($factory, $method->getDeclaringClass()->getParentClass()->getMethod($method->getName()));

            foreach ($method->getDeclaringClass()->getInterfaces() as $if)
                if ($if->hasMethod($method->getName()))
                    return self::getMethodSummary($factory, $if->getMethod($method->getName()));
        }

        // Map doccomment parameters
        $docPars = [];
        foreach ($doc->getTagsWithTypeByName("param") as $par)
            $docPars[$par->getVariableName()] = $par;

        return [$doc, $docPars];
    }

    private static function typeToReference(ReflectionType $type, string $namespace): Reference
    {
        $specs = [];
        if ($type instanceof ReflectionUnionType) {
            $shortType = "";
            $longType = "";
            $uidType = "";
            foreach ($type->getTypes() as $t) {
                if ($shortType != "") {
                    $shortType .= "|";
                    $longType .= "|";
                    $uidType .= "|";
                    $specs[] = new Reference("|");
                }

                if ($t->allowsNull()) {
                    $shortType .= "?";
                    $longType .= "?";
                    $uidType .= "?";
                    $specs[] = new Reference("?");
                }
                $short = explode("\\", $t->getName());
                $short = $short[count($short) - 1];
                $shortType .= $short;
                $longType .= $t->getName();
                $uidType .= self::classToUid($t->getName());
                $specs[] = self::typeToReference($t, $namespace);
            }
        } else {
            $shortType = explode("\\", $type->getName());
            $shortType = $shortType[count($shortType) - 1];
            $longType = $type->getName();
            $uidType = self::classToUid($longType);
        }

        $ns = explode("\\", $longType);
        $ns = substr($longType, 0, -strlen($ns[count($ns) - 1]) - 1);
        return new Reference($uidType, $shortType, "", $longType, $shortType, $specs, count($specs) == 0 && $ns != $namespace);
    }

    // endregion
}