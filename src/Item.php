<?php
/**
 * Copyright (c) Picorose
 * Licensed under the MIT license. See LICENSE file in the project root for full license information
 *
 * @author Colin Rosen
 * @date 2022
 * @since 1.0.0
 */

namespace com\Picorose\DocFx;

/**
 * Item is the basic unit of metadata format. From a documentation perspective, each item represents a "section" in the
 * reference documentation. This "section" is the minimum unit that you can cross reference to, or customize in layout
 * and content.
 *
 * Items can be hierarchical. One item can have other items as children. For example, in php, namespaces and classes can
 * have classes and/or methods as children.
 *
 * @link https://dotnet.github.io/docfx/spec/metadata_format_spec.html#11-items
 */
class Item
{
    use ArraySerialize;

    // region Fields

    private string $uid = "";
    private string $id = "";
    private string $parent = "";
    private array $children = [];
    private array $langs = [];
    private string $name = "";
    private string $nameWithType = "";
    private string $fullName = "";
    private string $type = "";
    private string $namespace = "";
    private array $assemblies = ["php"];
    private ?Syntax $syntax = null;
    private array $inheritance = [];
    private array $implements = [];
    private array $inheritedMembers = [];
    private string $overridden = "";
    private array $modifiers = [];
    private string $summary = "";

    // endregion

    // region Getters

    /**
     * Each item has a unique identifier (UID) which is globally unique.
     * A UID is defined as follows:
     * 1. If an item does not have a parent, its UID is its ID.
     * 2. Otherwise, its UID is the combination of the UID of its parent, a separator and the ID of the item itself.
     *
     * @link https://dotnet.github.io/docfx/spec/metadata_format_spec.html#12-identifiers
     * @return string A unique identifier for this item
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @link https://dotnet.github.io/docfx/spec/metadata_format_spec.html#23-item-object
     * @return string A unique identifier for this item, not including its parent's identifier
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * The UID of the item's parent. If omitted, metadata parser will try to figure out its parent from the children
     * information of other items within the same file.
     *
     * @link https://dotnet.github.io/docfx/spec/metadata_format_spec.html#23-item-section
     * @return string The UID of the parent item
     */
    public function getParent(): string
    {
        return $this->parent;
    }

    /**
     * @return string[] A list of UIDs of the item's children. Can be omitted if there are no children.
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @return string[] A list of supported programming languages for this object
     */
    public function getLangs(): array
    {
        return $this->langs;
    }

    /**
     * @link https://dotnet.github.io/docfx/spec/metadata_format_spec.html#23-item-object
     * @return string The display name of the item
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string The display name of the item including the base type of the item
     * E.g. for a method: Class1->myMethod()
     */
    public function getNameWithType(): string
    {
        return $this->nameWithType;
    }

    /**
     * @link https://dotnet.github.io/docfx/spec/metadata_format_spec.html#23-item-object
     * @return string The full display name of the item. In programming languages, it's usually the full qualified name.
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @link https://dotnet.github.io/docfx/spec/metadata_format_spec.html#23-item-object
     * @return string The type of the item, such as class, method, etc.
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string The namespace of the item. Only applicable for class type items.
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * @return Syntax Description of the item's syntax, i.e. its definition in the programming language, a list of its
     * parameters and return type with a description.
     */
    public function getSyntax(): Syntax
    {
        return $this->syntax;
    }

    /**
     * @return string[] A list of UIDs of the item's parent objects in ascending order of depth. Only applicable for
     * class type items.
     */
    public function getInheritance(): array
    {
        return $this->inheritance;
    }

    /**
     * @return string[] A list of UIDs of interfaces this item implements. Only applicable for class type items.
     */
    public function getImplements(): array
    {
        return $this->implements;
    }

    /**
     * @return string[] A list of UIDs of method items this item inherits from its parents. Only applicable for class
     * type items.
     */
    public function getInheritedMembers(): array
    {
        return $this->inheritedMembers;
    }

    /**
     * @return string The UID of the method item this item overrides. Only applicable for method type items.
     */
    public function getOverridden(): string
    {
        return $this->overridden;
    }

    /**
     * @return string[] A list of modifiers for this item. E.g. public, static, abstract
     */
    public function getModifiers(): array
    {
        return $this->modifiers;
    }

    /**
     * @return string A description of this item
     */
    public function getSummary(): string
    {
        return $this->summary;
    }

    // endregion

    // region Setters

    /**
     * @link https://dotnet.github.io/docfx/spec/metadata_format_spec.html#12-identifiers
     * @param string $uid A unique identifier for this item
     */
    public function setUid(string $uid)
    {
        $this->uid = $uid;
    }

    /**
     * @link https://dotnet.github.io/docfx/spec/metadata_format_spec.html#23-item-object
     * @param string $id A unique identifier for this item, not including its parent's identifier
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @link https://dotnet.github.io/docfx/spec/metadata_format_spec.html#23-item-section
     * @param string $parent The UID of the parent item
     */
    public function setParent(string $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @param string[] $children A list of UIDs of the item's children. Can be omitted if there are no children.
     */
    public function setChildren(array $children)
    {
        $this->children = $children;
    }

    /**
     * Adds a single child UID to this item
     *
     * @param string $child A UID of another item that should be added as a child to this item.
     */
    public function addChild(string $child)
    {
        $this->children[] = $child;
    }

    /**
     * @param string[] $langs A list of supported programming languages for this object
     */
    public function setLangs(array $langs)
    {
        $this->langs = $langs;
    }

    /**
     * @link https://dotnet.github.io/docfx/spec/metadata_format_spec.html#23-item-object
     * @param string $name The display name of the item
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $nameWithType The display name of the item including the base type of the item
     * E.g. for a method: Class1->myMethod()
     */
    public function setNameWithType(string $nameWithType)
    {
        $this->nameWithType = $nameWithType;
    }

    /**
     * @link https://dotnet.github.io/docfx/spec/metadata_format_spec.html#23-item-object
     * @param string $fullName The full display name of the item. In programming languages, it's usually the full
     * qualified name.
     */
    public function setFullName(string $fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @link https://dotnet.github.io/docfx/spec/metadata_format_spec.html#23-item-object
     * @param string $type The type of the item, such as class, method, etc.
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @param string $namespace The namespace of the item. Only applicable for class type items.
     */
    public function setNamespace(string $namespace)
    {
        $this->namespace = $namespace;
    }

    /**
     * @param Syntax $syntax Description of the item's syntax, i.e. its definition in the programming language, a list
     * of its parameters and return type with a description.
     */
    public function setSyntax(Syntax $syntax)
    {
        $this->syntax = $syntax;
    }

    /**
     * @param string[] $inheritance A list of UIDs of the item's parent objects in ascending order of depth. Only
     * applicable for class type items.
     */
    public function setInheritance(array $inheritance)
    {
        $this->inheritance = $inheritance;
    }

    /**
     * Adds a single UID of an item to the end of the inheritance list of this item
     *
     * @param string $class The UID of the item to add to the inheritance list of this item
     */
    public function addInheritance(string $class)
    {
        array_unshift($this->inheritance, $class);
    }

    /**
     * @param string[] $implements A list of UIDs of interfaces this item implements. Only applicable for class type
     * items.
     */
    public function setImplements(array $implements)
    {
        $this->implements = $implements;
    }

    /**
     * @param string[] $inheritedMembers A list of UIDs of method items this item inherits from its parents. Only
     * applicable for class type items.
     */
    public function setInheritedMembers(array $inheritedMembers)
    {
        $this->inheritedMembers = $inheritedMembers;
    }

    /**
     * @param string $overridden The UID of the method item this item overrides. Only applicable for method type items.
     */
    public function setOverridden(string $overridden)
    {
        $this->overridden = $overridden;
    }

    /**
     * @param string[] $modifiers A list of modifiers for this item. E.g. public, static, abstract.
     */
    public function setModifiers(array $modifiers)
    {
        $this->modifiers = $modifiers;
    }

    /**
     * Adds a single modifier to the list of modifiers for this item.
     *
     * @param string $mod A modifier for this item. E.g. public, static, abstract.
     */
    public function addModifier(string $mod)
    {
        $this->modifiers[] = $mod;
    }

    /**
     * @param string $summary A description of this item
     */
    public function setSummary(string $summary)
    {
        $this->summary = $summary;
    }

    // endregion
}